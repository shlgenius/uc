package kr.co.uclick.service;

import java.util.ArrayList;

import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import kr.co.uclick.configuration.SpringConfiguration;
import kr.co.uclick.entity.Member;
import kr.co.uclick.entity.Phone;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes = SpringConfiguration.class)
public class PhoneServiceTest {
	
	@Autowired
	PhoneService phoneService;
	
	@Ignore
	@Test
	public void addTest() {
		phoneService.add(5, "01000060001");
	}
	
	@Ignore
	@Test
	public void updateTest() {
		phoneService.update(8, "01000060011");
	}
	
	@Ignore
	@Test
	public void findByNoContainingTest() {
		ArrayList<Phone> phone = phoneService.findByNoContaining("2");
		ArrayList<Member> member = new ArrayList<Member>();
		
		for(int i = 0; i < phone.size(); i++) {
			member.add(phone.get(i).getMember());
			System.out.println("member : " + phone.get(i).getMember());
			System.out.println("member : " + member.get(i).getName());
		}		
	}
	
	@Ignore
	@Test
	public void deleteTest() {
		phoneService.delete(8);
	}
	
	@Ignore
	@Test
	public void findAllByIdTest() {
		ArrayList<Phone> phone = phoneService.findAllByMemberId(1);
		for (int i = 0; i < phone.size(); i++) {			
			System.out.println("phoneNo. : " + phone.get(i).getNo());
		}
	}

	@Ignore
	@Test
	public void genTest() {
		phoneService.genUpdate(41, 42);		
	}
}
