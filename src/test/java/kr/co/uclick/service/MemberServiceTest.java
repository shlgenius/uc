package kr.co.uclick.service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;


import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import kr.co.uclick.configuration.SpringConfiguration;
import kr.co.uclick.entity.Member;
import kr.co.uclick.entity.Phone;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes = SpringConfiguration.class)
public class MemberServiceTest {

	@Autowired
	MemberService memberService;
	
	@Ignore
	@Test
	public void createTest() {
		memberService.create("Kim Munju");
	}
	
	@Ignore
	@Test
	public void createNoTest() {
		memberService.createNo("Lee Hanjun", "01000060001");
	}
	
	@Ignore
	@Test
	public void updateTest() {
		memberService.update(7, "Wang Taeyoung");
	}
	
	@Ignore
	@Test
	public void deleteTest() {
		memberService.delete(7);
	}
	
	@Autowired
	PhoneService phoneService;
	
	@Ignore
	@Test
	@Transactional
	public void getTest() {
		
		ArrayList<Phone> phone = phoneService.findAllByMemberId(1);		
		List<Member> member = memberService.getById(1);		
		
		for(int i = 0; i < member.size(); i++) {
			System.out.println("member : " + member.get(i).getName());
			
			List<Phone> list = member.get(i).getPhones().stream().collect(Collectors.toList());
			
			for (int k = 0; k < list.size(); k++) {
				System.out.println(list.get(k).getNo());	
			}
			
			for (int j = 0; j < phone.size(); j++) {
				System.out.println("phoneNo. : " + phone.get(j).getNo());
			}			
			
		}		
	}
}
