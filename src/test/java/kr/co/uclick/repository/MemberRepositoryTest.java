package kr.co.uclick.repository;

import java.util.List;

import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.util.StopWatch;

import kr.co.uclick.configuration.SpringConfiguration;
import kr.co.uclick.entity.Member;
import kr.co.uclick.entity.Phone;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes = SpringConfiguration.class)
public class MemberRepositoryTest {

	@Autowired
	MemberRepository memberRepository;

	@Autowired
	PhoneRepository phoneRepository;

	@Ignore
	@Test
	public void oneToManyTwoWay() {
		Member first = new Member("Lee Jungwan");
		first.addPhone(new Phone("01000010001"));
		first.addPhone(new Phone("01000010002"));

		Member second = new Member("Ko Euisuk");
		second.addPhone(new Phone("01000020001"));

		Member third = new Member("Park Seungchan");

		memberRepository.save(first);
		memberRepository.save(second);
		memberRepository.save(third);

		List<Member> memberList = memberRepository.findAll();

		for (Member m : memberList) {
			System.out.println(m.toString());
			System.out.println(m.getId() + ", " + m.getName());
		}
	}

//	@Ignore
//	@Test
//	public void findAllByName() {
//		List<Member> name = memberRepository.findMemberByName("Lee Jung Wan");	// 정확히 일치해야 
//		System.out.println("name : " + name);		
//	}
//	
//	@Ignore
//	@Test
//	public void findMemberByNameLike() {
//		List<Member> name = memberRepository.findMemberByNameLike("%Lee%");		// like
//		System.out.println("name : " + name);
//	}

	@Ignore
	@Test
	public void findByNameContaining() {
		
		StopWatch sw = new StopWatch();
		sw.start();		
		List<Member> name1 = memberRepository.findByNameContaining("e"); // like		
		sw.stop();
		for (int i = 0; i < name1.size(); i++) {
			System.out.println("name : " + name1.get(i).getName());
		}
		System.out.println("1 total time : " + sw.getTotalTimeSeconds());
		
		sw = new StopWatch();
		sw.start();		
		List<Member> name2 = memberRepository.findByNameContaining("e"); // like
		sw.stop();
		for (int i = 0; i < name1.size(); i++) {
			System.out.println("name : " + name2.get(i).getName());
		}
		System.out.println("2 total time : " + sw.getTotalTimeSeconds());
		
		sw = new StopWatch();
		sw.start();		
		List<Member> name3 = memberRepository.findByNameContaining("e"); // like
		sw.stop();
		for (int i = 0; i < name1.size(); i++) {
			System.out.println("name : " + name3.get(i).getName());
		}
		System.out.println("3 total time : " + sw.getTotalTimeSeconds());
		
	}

	@Ignore
	@Test
	public void createTest() {
		Member fourth = new Member("Shin Munkyu");

		Phone p = new Phone("01000040001");
		p.setMember(fourth);
		fourth.addPhone(p);

		memberRepository.save(fourth);

		List<Member> memberList = memberRepository.findAll();
		for (Member m : memberList) {
			System.out.println(m.toString());
			System.out.println(m.getId());
			System.out.println(m.getName());
		}
	}

	@Ignore
	@Test
	public void readTest() {
		List<Member> members = memberRepository.findByNameContaining("e"); // like
		for(Member m : members) {
			System.out.println("id : " + m.getId() + "  name : " + m.getName());
		}		
	}

	@Ignore
	@Test
	public void updateTest() { // update memberName
		Member member = memberRepository.findById(4L).get();
		member.setName("Lee Hyunsuk");
		memberRepository.save(member);
		
		System.out.println("id : " + member.getId());
		System.out.println("name : " + member.getName());
	}

	@Ignore
	@Test
	public void deleteTest() {
		memberRepository.deleteById(4L);
	}
	
	@Ignore
	@Test
	public void cacheTest() {
//		memberRepository.findAll();
//		memberRepository.findAll();
//		memberRepository.findAll();
		
		memberRepository.findById(2L).get();
		memberRepository.findById(2L).get();
		memberRepository.findById(2L).get();
	}
	
}
