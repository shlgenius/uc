package kr.co.uclick.repository;

import java.util.ArrayList;

import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.util.StopWatch;

import kr.co.uclick.configuration.SpringConfiguration;
import kr.co.uclick.entity.Member;
import kr.co.uclick.entity.Phone;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes = SpringConfiguration.class)
public class PhoneRepositoryTest {

	@Autowired
	MemberRepository memberRepository;

	@Autowired
	PhoneRepository phoneRepository;
	
	@Ignore
	@Test
	public void addNum() {	// add phoneNumber
		Member member = memberRepository.findById(5L).get();
		if (member == null) {
			
		} else {
			Phone phone = new Phone();			
			phone.setNo("01000040023");
			phone.setMember(member);
			phoneRepository.save(phone);
		}
	}
	
	@Ignore
	@Test
	public void updateTest() { // update phoneNum
		Phone phone = phoneRepository.findById(5L).get();
		phone.setNo("01000040021");
		phoneRepository.save(phone);
	}
	
	@Ignore
	@Test
	public void findAllByMemberIdTest() {
		ArrayList<Phone> phone = phoneRepository.findAllByMemberId(5L);
		for(int i = 0; i < phone.size(); i++) {
			System.out.println("Member" + i + " : " + phone.get(i).getMember().getName());
			System.out.println("Number" + i + " : " + phone.get(i).getNo());			
		}
	}
	
	@Ignore
	@Test
	public void findAllMemberSearchByNumber() {
		
		StopWatch sw = new StopWatch();
		sw.start();		
		ArrayList<Phone> phone = phoneRepository.findByNoContaining("010");
		sw.stop();
		ArrayList<Member> member = new ArrayList<Member>();		
		for(int i = 0; i < phone.size(); i++) {
			member.add(phone.get(i).getMember());
			System.out.println("member : " + phone.get(i).getMember());
			System.out.println("member : " + member.get(i).getName());
		}
		System.out.println("1 total time : " + sw.getTotalTimeSeconds());
		
		sw = new StopWatch();
		sw.start();
		ArrayList<Phone> phone2 = phoneRepository.findByNoContaining("010");
		sw.stop();
		ArrayList<Member> member2 = new ArrayList<Member>();		
		for(int i = 0; i < phone2.size(); i++) {
			member2.add(phone2.get(i).getMember());
			System.out.println("member : " + phone2.get(i).getMember());
			System.out.println("member : " + member2.get(i).getName());
		}
		System.out.println("2 total time : " + sw.getTotalTimeSeconds());
		
		sw = new StopWatch();
		sw.start();
		ArrayList<Phone> phone3 = phoneRepository.findByNoContaining("010");
		sw.stop();
		ArrayList<Member> member3 = new ArrayList<Member>();		
		for(int i = 0; i < phone3.size(); i++) {
			member3.add(phone3.get(i).getMember());
			System.out.println("member : " + phone3.get(i).getMember());
			System.out.println("member : " + member3.get(i).getName());
		}
		System.out.println("3 total time : " + sw.getTotalTimeSeconds());
	}
	
	@Ignore
	@Test
	public void deleteTest() {	// delete phoneNum
		phoneRepository.deleteById(6L);
	}
	
	@Ignore
	@Test
	public void testing() {
//		Member member = memberRepository.findById(5L).get();
		Phone phone = phoneRepository.findById(5L).get();
//		int gen = phoneRepository.findById(5L).get().getGen();
		
//		Phone p = phoneRepository.findByGenAndMember(1, member).get(0);
		
//		System.out.println("member : " + member.getName());		
//		System.out.println("phoneNo : " + phone.getNo());
//		System.out.println("phoneGen : " + phone.getGen());
//		System.out.println("phoneGen : " + gen);
		
		ArrayList<Phone> p = phoneRepository.findByGenAndMember(0, phone.getMember());
//		phoneRepository.findByGenAndMember(1, phone.getMember()).get(0);
		for (int i = 0; i < p.size(); i++) {
			System.out.println(p.get(i).getNo());
		}

	}

}
