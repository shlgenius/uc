package kr.co.uclick.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import kr.co.uclick.dto.MemberRequestDto;
import kr.co.uclick.dto.MemberResponseDto;
import kr.co.uclick.dto.PhoneRequestDto;
import kr.co.uclick.dto.PhoneResponseDto;
import kr.co.uclick.entity.Member;
import kr.co.uclick.entity.Phone;
import kr.co.uclick.service.MemberService;
import kr.co.uclick.service.PhoneService;

//@Transactional
@Controller
public class MemberController {

	@Autowired
	private MemberService memberService;

	@Autowired
	private PhoneService phoneService;

//	@Transactional
	@RequestMapping(value = "list")
	public String list(Model model, @RequestParam HashMap<String, String> map) {

		String key = map.get("key");
		String search = map.get("searchString");

//		List<Member> members = memberService.findAllByOrderByIdDesc();	// ID descending
//		List<Member> members = memberService.findAll(); // ID ascending
		List<MemberResponseDto> members = memberService.findAll();

		if (key == null || key.equals("") || search == null || search.equals("")) {
//			members = memberService.findAll();	
			members = memberService.findAll();
		} else if (key.equals("name")) {
//			members = memberService.findByNameContaining(search);
			members = memberService.searchByName(search);
		} else if (key.equals("no")) {
//			members = phoneService.findAllMemberByNo(search);
			members = phoneService.searchAllMemberByNo(search);
		} else if (key.equals("genNo")) {
//			members = phoneService.findAllMemberByGenAndNo(1, search);
			members = phoneService.searchAllMemberByGenAndNo(1, search);
		}

//		for (MemberResponseDto m : members) {
//			Hibernate.initialize(m.getPhones());
//		}
		model.addAttribute("members", members);

		return "list";
	}

	@RequestMapping(value = "view")
	public String view(Model model, int id) {

		List<Member> members = memberService.getById(id);
		ArrayList<Phone> phones = phoneService.findAllByMemberId(id);

		model.addAttribute("viewMembers", members);
		model.addAttribute("viewPhones", phones);

		return "view";
	}

//	saveDto에서 return한 에러메세지를 받아서 새로운 모델에 삽입
	@RequestMapping(value = "newForm")
	public String newForm(Model model, @RequestParam HashMap<String, String> map) {
		Object err0 = map.get("err0");
		if (err0 != null) {
			model.addAttribute("err0", err0);
		}
		Object err4 = map.get("err4");
		if (err4 != null) {
			model.addAttribute("err4", err4);
		}
		return "newForm";
	}

	@RequestMapping(value = "save")
	public String save(Model model, @RequestParam HashMap<String, String> map) {

		String name = map.get("name");
		String no = map.get("no");

		if (no.length() == 0) {
			memberService.create(name);
		} else {
			memberService.createNo(name, no);
		}

		return "redirect:list";
	}

	@RequestMapping(value = "editForm")
	public String editForm(Model model, @RequestParam HashMap<String, String> map) {

		Object err = map.get("err");
		if (err != null) {
			model.addAttribute("err", err);
		}

		int id = Integer.parseInt(map.get("id"));

		List<Member> members = memberService.getById(id);
		ArrayList<Phone> phones = phoneService.findAllByMemberId(id);

		model.addAttribute("viewMembers", members);
		model.addAttribute("viewPhones", phones);

		return "editForm";
	}

	@RequestMapping(value = "memberUpdate")
	public String update(Model model, @RequestParam HashMap<String, String> map) {

		int memberId = Integer.parseInt(map.get("memberId"));
		String name = map.get("name");

		memberService.update(memberId, name);

//		return "redirect:view?id=" + memberId;
		return "redirect:viewDto?id=" + memberId;
	}

	@RequestMapping(value = "memberDelete")
	public String deleteMember(Model model, int id) {
		memberService.delete(id);
		return "redirect:list";
	}

	@RequestMapping(value = "phoneSave")
	public String addNo(Model model, @RequestParam HashMap<String, String> map) {

		int memberId = Integer.parseInt(map.get("memberId"));
		String no = map.get("no");

		phoneService.add(memberId, no);

//		return "redirect:view?id=" + memberId;
		return "redirect:viewDto?id=" + memberId;
	}

	@RequestMapping(value = "phoneAddForm")
	public String phoneAddForm(Model model, @RequestParam HashMap<String, String> map) {

		int memberId = Integer.parseInt(map.get("memberId"));

		Member member = memberService.findById(memberId);
		ArrayList<Phone> phones = phoneService.findAllByMemberId(memberId);

		model.addAttribute("phones", phones);
		model.addAttribute("member", member);

		return "phoneAddForm";
	}

	@RequestMapping(value = "phoneDelete")
	public String deleteNo(Model model, @RequestParam HashMap<String, String> map) {

		int phoneId = Integer.parseInt(map.get("phoneId"));
		int memberId = Integer.parseInt(map.get("memberId"));

		phoneService.delete(phoneId);

		return "redirect:viewDto?id=" + memberId;
	}

	@RequestMapping(value = "phoneUpdate")
	public String updateNo(Model model, @RequestParam HashMap<String, String> map) {

		int memberId = Integer.parseInt(map.get("memberId"));
		int phoneId = Integer.parseInt(map.get("phoneId"));
		String no = map.get("no");

		phoneService.update(phoneId, no);

//		return "redirect:view?id=" + memberId;
		return "redirect:viewDto?id=" + memberId;
	}

	@RequestMapping(value = "genUpdate")
	public String genUpdate(@RequestParam HashMap<String, String> map) {

		int memberId = Integer.parseInt(map.get("memberId"));
		int phoneId = Integer.parseInt(map.get("phoneId"));

		phoneService.genUpdate(memberId, phoneId);
		memberService.genUpdate(memberId, phoneId);

//		return "redirect:view?id=" + memberId;
		return "redirect:viewDto?id=" + memberId;
	}

//	Dto 

//	newForm -> saveDto -> newForm
//	saveDto(err message) -> return : newForm 
	@RequestMapping(value = "saveDto")
	public String saveMemberDto(@ModelAttribute("memberRequestDto") @Valid MemberRequestDto memberRequestDto, 
			BindingResult errors, Model model) {
		if (errors.hasErrors()) {
			List<ObjectError> list = errors.getAllErrors();
			model.addAttribute("err0", list.get(0));
			return "newForm";
		} else {
			if (phoneService.existsByNo(memberRequestDto.getNo()) == true) {
				String errMsg = "중복 번호입니다.";
				model.addAttribute("err4", errMsg);
				return "newForm";
			} else {
				memberService.save(memberRequestDto);
				return "redirect:list";
			}
		}
	}

	@RequestMapping(value = "viewDto")
	public String viewDto(Model model, int id) {

		MemberResponseDto members = memberService.findOne(id);
		ArrayList<PhoneResponseDto> phones = phoneService.findAllPhonesByMemberId(id);

		model.addAttribute("member", members);
		model.addAttribute("phones", phones);

		return "viewDto";
	}

	@RequestMapping(value = "editFormDto")
	public String editFormDto(Model model, @RequestParam HashMap<String, String> map) {

		Object err = map.get("err");
		if (err != null) {
			model.addAttribute("err", err);
		}
		Object err1 = map.get("err1");
		if (err1 != null) {
			model.addAttribute("err1", err1);
		}
		Object err2 = map.get("err2");
		if (err2 != null) {
			model.addAttribute("err2", err2);
		}

		int id = Integer.parseInt(map.get("id"));

		MemberResponseDto member = memberService.findOne(id);
//		ArrayList<PhoneResponseDto> phones = phoneService.findAllPhonesByMemberId(id);
		ArrayList<Phone> phones = phoneService.findAllByMemberId(id);

		model.addAttribute("member", member);
		model.addAttribute("phones", phones);

		return "editFormDto";
	}

	@RequestMapping(value = "memberUpdateDto")
	public String memberUpdateDto(@ModelAttribute("memberRequestDto") @Valid MemberRequestDto memberRequestDto,
			BindingResult errors, Model model) {
		if (errors.hasErrors()) {
			List<ObjectError> list = errors.getAllErrors();
			model.addAttribute("err", list.get(0).getDefaultMessage());
			return "redirect:editFormDto?id=" + memberRequestDto.getId().intValue();
		} else {
			memberService.update(memberRequestDto.getId().intValue(), memberRequestDto.getName());
			return "redirect:viewDto?id=" + memberRequestDto.getId();
		}
	}

	@RequestMapping(value = "phoneAddDto")
	public String phoneAddDto(@ModelAttribute @Valid PhoneRequestDto phoneRequestDto, BindingResult errors, Model model,
			@RequestParam HashMap<String, String> map) {

		Object err3 = map.get("err3");
		if (err3 != null) {
			model.addAttribute("err3", err3);
		}
		Object err4 = map.get("err4");
		if (err4 != null) {
			model.addAttribute("err4", err4);
		}

		int memberId = phoneRequestDto.getId().intValue();

		MemberResponseDto member = memberService.findOne(memberId);
//		ArrayList<PhoneResponseDto> phones = phoneService.findAllPhonesByMemberId(memberId);
		ArrayList<Phone> phones = phoneService.findAllByMemberId(memberId);

		model.addAttribute("phones", phones);
		model.addAttribute("member", member);

		return "phoneAddDto";
	}

	@RequestMapping(value = "phoneSaveDto")
	public String phoneSaveDto(@ModelAttribute("phoneRequestDto") @Valid PhoneRequestDto phoneRequestDto,
			BindingResult errors, Model model) {
		if (errors.hasErrors()) {
			List<ObjectError> list = errors.getAllErrors();
			model.addAttribute("err3", list.get(0).getDefaultMessage());
			return "redirect:phoneAddDto?id=" + phoneRequestDto.getMemberId().intValue();
		} else {
			if (phoneService.existsByNo(phoneRequestDto.getNo()) == true) {
				String errMsg = "중복 번호입니다.";
				model.addAttribute("err4", errMsg);
				return "redirect:phoneAddDto?id=" + phoneRequestDto.getMemberId().intValue();
			} else {
				phoneService.save(phoneRequestDto);
				return "redirect:viewDto?id=" + phoneRequestDto.getMemberId().intValue();
			}
		}
	}

	@RequestMapping(value = "phoneUpdateDto")
	public String phoneUpdateDto(@ModelAttribute("phoneUpdateDto") @Valid PhoneRequestDto phoneRequestDto,
			BindingResult errors, Model model, @RequestParam HashMap<String, String> map) {

		int memberId = Integer.parseInt(map.get("memberId"));

		if (errors.hasErrors()) {
			List<ObjectError> list = errors.getAllErrors();
			model.addAttribute("err1", list.get(0).getDefaultMessage());
			return "redirect:editFormDto?id=" + memberId;
		} else {
			if (phoneService.existsByNo(phoneRequestDto.getNo()) == true) {
				String errMsg = "중복 번호입니다.";
				model.addAttribute("err2", errMsg);
				model.addAttribute("url", "editFormDto?id=" + memberId);
				return "redirect:editFormDto?id=" + memberId;
//				return "error";
			} else {
				phoneService.update(phoneRequestDto.getId().intValue(), phoneRequestDto.getNo());
				return "redirect:editFormDto?id=" + memberId;
			}
		}
	}

}
