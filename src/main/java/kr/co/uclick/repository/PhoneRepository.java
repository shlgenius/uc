package kr.co.uclick.repository;

import java.util.ArrayList;

import javax.persistence.QueryHint;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.QueryHints;
import org.springframework.stereotype.Repository;

import kr.co.uclick.dto.PhoneResponseDto;
import kr.co.uclick.entity.Member;
import kr.co.uclick.entity.Phone;

@Repository
public interface PhoneRepository extends JpaRepository<Phone, Long> {

	public ArrayList<Phone> findAllById(long id);

	public ArrayList<Phone> findAllByMemberId(long memberId);

	public ArrayList<Phone> findByMember(Member member);

	@QueryHints(value = {
			@QueryHint(name = "org.hibernate.cacheable", value = "true"),
			@QueryHint(name = "org.hibernate.cacheMode", value = "NORMAL")})
	public ArrayList<Phone> findByNoContaining(String no);

	public ArrayList<Phone> findByGenAndMember(int gen, Member member);

	@QueryHints(value = {
			@QueryHint(name = "org.hibernate.cacheable", value = "true"),
			@QueryHint(name = "org.hibernate.cacheMode", value = "NORMAL")})
	public ArrayList<Phone> findByGenAndNoContaining(int gen, String no);

	public ArrayList<PhoneResponseDto> findByMemberId(long memberId);

	public boolean existsByNo(String no);
}
