package kr.co.uclick.repository;

import java.util.List;

import javax.persistence.QueryHint;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.QueryHints;
import org.springframework.stereotype.Repository;

import kr.co.uclick.entity.Member;

@Repository
public interface MemberRepository extends JpaRepository<Member, Long> {
	
	@QueryHints(value = {
			@QueryHint(name = "org.hibernate.cacheable", value = "true"),
			@QueryHint(name = "org.hibernate.cacheMode", value = "NORMAL")})
	public List<Member> findByNameContaining(String name);

	public List<Member> findAllByOrderByIdDesc();
	
	public List<Member> getById(long id);

	public void deleteGenNoById(Long id);
	
}
