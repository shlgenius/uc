package kr.co.uclick.dto;

import kr.co.uclick.entity.Member;
import kr.co.uclick.entity.Phone;

public class PhoneResponseDto {

	private Long id;
		
	private String no;
	
	private int gen;
	
	private String genPhoneNo;
	
	private Member member;
	
	public PhoneResponseDto() {}
	
	public PhoneResponseDto(Phone phone) {
		this.id = phone.getId();
		this.no = phone.getNo();
		this.gen = phone.getGen();
		this.member = phone.getMember();
		
		String num = phone.getNo();
//		int mid = num.length() == 10? 6 : 7;
//		num = num.substring(0, 3) + "-" + num.substring(3, mid) + "-" + num.substring(mid, num.length());
		
		if (num.length() == 9) {
			num = num.substring(0, 2) + "-" + num.substring(2, 5) + "-" + num.substring(5, num.length());
		} else if (num.length() == 10) {
			num = num.substring(0, 3) + "-" + num.substring(3, 6) + "-" + num.substring(6, num.length());
		} else if (num.length() == 11) {
			num = num.substring(0, 3) + "-" + num.substring(3, 7) + "-" + num.substring(7, num.length());
		}
		
		this.no = num;
		
		if (this.gen == 1) {
			this.genPhoneNo = this.no;
		}		
	}

	public Long getId() {
		return id;
	}
	
	public String getNo() {
		return no;
	}

	public int getGen() {
		return gen;
	}
	
	public String getGenPhoneNo() {
		return genPhoneNo;
	}

	public Member getMember() {
		return member;
	}
	
}
