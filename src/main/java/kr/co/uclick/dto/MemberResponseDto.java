package kr.co.uclick.dto;

import kr.co.uclick.entity.Member;

public class MemberResponseDto {

	private Long id;

	private String name;

	private String genNo;

	public MemberResponseDto() {
	}

	public MemberResponseDto(Member member) {
		this.id = member.getId();
		this.name = member.getName();
		if (member.getGenNo() != null) {
			String num = member.getGenNo();
			if (num.length() == 9) {
				num = num.substring(0, 2) + "-" + num.substring(2, 5) + "-" + num.substring(5, num.length());
			} else if (num.length() == 10) {
				num = num.substring(0, 3) + "-" + num.substring(3, 6) + "-" + num.substring(6, num.length());
			} else if (num.length() == 11) {
				num = num.substring(0, 3) + "-" + num.substring(3, 7) + "-" + num.substring(7, num.length());
			}
			this.genNo = num;
		} else {
			this.genNo = member.getGenNo();
		}
	}

	public Long getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public String getGenNo() {
		return genNo;
	}

}
