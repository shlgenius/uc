package kr.co.uclick.dto;

import javax.validation.constraints.Pattern;

import org.springframework.beans.factory.annotation.Autowired;

import kr.co.uclick.entity.Phone;
import kr.co.uclick.repository.MemberRepository;

public class PhoneRequestDto {
	
	@Autowired
	MemberRepository memberRepository;
	
	private Long id;
	
	@Pattern(regexp = "[0-9]{9,11}", message = "9~11자리의 숫자만 입력해 주세요.")
	private String no;
	
	private Long memberId;
	
	private int gen;
	
	public PhoneRequestDto() {}
	
	public PhoneRequestDto(long memberId, int gen, String no) {
		this.memberId = memberId;
		this.gen = gen;
		this.no = no;
	}
	
	public Phone toEntity() {
		Phone phone = new Phone(no);
		phone.setGen(gen);
		return phone;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNo() {
		return no;
	}

	public void setNo(String no) {
		this.no = no;
	}

	public Long getMemberId() {
		return memberId;
	}

	public void setMemberId(Long memberId) {
		this.memberId = memberId;
	}

	public int getGen() {
		return gen;
	}

	public void setGen(int gen) {
		this.gen = gen;
	}
	
}
