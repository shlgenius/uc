package kr.co.uclick.dto;

import java.util.ArrayList;
import java.util.List;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;

import org.hibernate.validator.constraints.Length;

import kr.co.uclick.entity.Member;
import kr.co.uclick.entity.Phone;

public class MemberRequestDto {

	private Long id;

	@NotBlank(message = "이름을 작성해 주세요.")
	@Length(max = 20, message = "20자 이하 입력")
//	@Pattern(regexp = "^[a-zA-Z가-힣]*$", message = "문자만 입력 가능")
	@Pattern(regexp = "^[가-힣]{2,9}|[a-zA-Z]{2,9}\\s[a-zA-Z]{2,9}$", message = "한글 10자/영문 19자 이하입력, ex) 홍길동, Michael Jordan")
	private String name;

	@Pattern(regexp = "[0-9]{9,11}", message = "9~11자리의 숫자만 입력해 주세요.")
	private String no;

	private List<String> phones;

	public MemberRequestDto() {}

	public MemberRequestDto(String name, String no) {
		this.name = name;
		if (no != null) {
			this.no = no;
		}
	}
	
	public MemberRequestDto(String name) {
		this.name = name;
	}

	public Member toEntity() {
		Member member = new Member(name);
//		Member member = null;
		if (no != null) {
			member = new Member(name);
			member.addPhone(new Phone(no));
		}
		return member;
	}

	public Long getId() {
		return id;
	}
	
	public void setId(Long id) {
		this.id = id;
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public List<String> getPhones() {
		if (phones == null) {
			phones = new ArrayList<String>();
		}
		return phones;
	}
	
	public void setPhones(List<String> phones) {
		this.phones = phones;
	}
	
	public String getNo() {
		return no;
	}
	
	public void setNo(String no) {
		this.no = no;
	}

}
