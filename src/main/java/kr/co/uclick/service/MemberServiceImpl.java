package kr.co.uclick.service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import kr.co.uclick.dto.MemberRequestDto;
import kr.co.uclick.dto.MemberResponseDto;
import kr.co.uclick.entity.Member;
import kr.co.uclick.entity.Phone;
import kr.co.uclick.repository.MemberRepository;
import kr.co.uclick.repository.PhoneRepository;

@Service
public class MemberServiceImpl implements MemberService {

	@Autowired
	MemberRepository memberRepository;

	@Autowired
	PhoneRepository phoneRepository;

	@Override
	public List<Member> findAllByOrderByIdDesc() {
		// TODO Auto-generated method stub
		return memberRepository.findAllByOrderByIdDesc();
	}

//	@Override
//	public List<Member> findAll() {
//		return memberRepository.findAll();
//	}

	@Override
	public List<Member> findByNameContaining(String name) {
		// TODO Auto-generated method stub
		return memberRepository.findByNameContaining(name);
	}

	@Override
	public void create(String name) {
		// TODO Auto-generated method stub
		Member member = new Member();
		member.setName(name);
		memberRepository.save(member);
	}

	@Override
	public void update(int id, String name) {
		// TODO Auto-generated method stub
		Member member = memberRepository.findById((long) id).get();
//		Member member = memberRepository.getOne((long) id);
		member.setName(name);
		memberRepository.save(member);
	}

	@Override
	public void delete(int id) {
		// TODO Auto-generated method stub
		memberRepository.deleteById((long) id);
	}

	@Override
	public void createNo(String name, String no) {
		// TODO Auto-generated method stub
		Member member = new Member();
		member.setName(name);

		Phone phone = new Phone(no);
		member.addPhone(phone);

		memberRepository.save(member);
	}

	@Override
	public List<Member> getById(int id) {
		// TODO Auto-generated method stub
		return memberRepository.getById((long) id);
	}

	@Override
	public Member findById(int id) {
		// TODO Auto-generated method stub
		Optional<Member> mem = memberRepository.findById((long) id);
		Member member = mem.get();
		return member;
	}

//	Dto
	@Override
	public void save(MemberRequestDto member) {
		// TODO Auto-generated method stub
		memberRepository.save(member.toEntity());
	}

	@Override
	public MemberResponseDto findOne(int id) {
		// TODO Auto-generated method stub
		MemberResponseDto member = new MemberResponseDto(memberRepository.findById((long) id).get()); // return : proxy
//		memberRepository.getOne((long)id);	// return : reference
		return member;
	}

	@Override
	public List<MemberResponseDto> findAll() {
		// TODO Auto-generated method stub
		List<MemberResponseDto> members = memberRepository.findAll().stream().map(MemberResponseDto::new)
				.collect(Collectors.toList());
		return members;
	}

	@Override
	public void genUpdate(int memberId, int phoneId) {
		// TODO Auto-generated method stub
		Member member = memberRepository.findById((long) memberId).get();
		String newGenNo = phoneRepository.findById((long) phoneId).get().getNo();
		member.setGenNo(newGenNo);
		memberRepository.save(member);
	}

	@Override
	public List<MemberResponseDto> searchByName(String name) {
		// TODO Auto-generated method stub
		List<MemberResponseDto> members = memberRepository.findByNameContaining(name).stream()
				.map(MemberResponseDto::new).collect(Collectors.toList());
		return members;
	}

}
