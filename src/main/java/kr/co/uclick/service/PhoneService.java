package kr.co.uclick.service;

import java.util.ArrayList;
import java.util.List;

import kr.co.uclick.dto.MemberResponseDto;
import kr.co.uclick.dto.PhoneRequestDto;
import kr.co.uclick.dto.PhoneResponseDto;
import kr.co.uclick.entity.Member;
import kr.co.uclick.entity.Phone;

public interface PhoneService {

	public ArrayList<Phone> findByNoContaining(String no);
	
	public void add(int memberId, String no);
	public void update(int id, String no);
	public void delete(int id);
	
	public ArrayList<Phone> findAllByMemberId(int memberId);
	
	public List<Member> findAllMemberByNo(String no);
	
	public List<Member> findAllMemberByGenAndNo(int gen, String no);
	
	public void genUpdate(int memberId, int phoneId);
		
//	Dto
	public ArrayList<PhoneResponseDto> findAllPhonesByMemberId(int memberId);
	
	public List<MemberResponseDto> searchAllMemberByNo(String no);
	
	public List<MemberResponseDto> searchAllMemberByGenAndNo(int gen, String no);
	
	public void save(PhoneRequestDto phone);
	
	public boolean existsByNo(String no);
	
}
