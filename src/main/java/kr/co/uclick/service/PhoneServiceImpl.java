package kr.co.uclick.service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import kr.co.uclick.dto.MemberResponseDto;
import kr.co.uclick.dto.PhoneRequestDto;
import kr.co.uclick.dto.PhoneResponseDto;
import kr.co.uclick.entity.Member;
import kr.co.uclick.entity.Phone;
import kr.co.uclick.repository.MemberRepository;
import kr.co.uclick.repository.PhoneRepository;

@Service
public class PhoneServiceImpl implements PhoneService {

	@Autowired
	MemberRepository memberRepository;

	@Autowired
	PhoneRepository phoneRepository;

	@Override
	public ArrayList<Phone> findByNoContaining(String no) {
		// TODO Auto-generated method stub
		return phoneRepository.findByNoContaining(no);
	}

	@Override
	public void add(int memberId, String no) {
		// TODO Auto-generated method stub
//		Member member = memberRepository.findById(memberId).get();
		Member member = memberRepository.getOne((long) memberId);
		Phone phone = new Phone();
		phone.setNo(no);
		phone.setMember(member);
		phoneRepository.save(phone);
	}

	@Override
	public void update(int id, String no) {
		// TODO Auto-generated method stub
		Phone phone = phoneRepository.findById((long) id).get();
		phone.setNo(no);
		phoneRepository.save(phone);
	}

	@Override
	public void delete(int id) {
		// TODO Auto-generated method stub
		Phone phone = phoneRepository.findById((long) id).get();
		int gen = phone.getGen();
		if (gen == 1) {
			Member member = memberRepository.findById(phone.getMember().getId()).get();
			member.setGenNo(null);
			memberRepository.save(member);
		}
		phoneRepository.deleteById((long) id);
	}

	@Override
	public ArrayList<Phone> findAllByMemberId(int memberId) {
		// TODO Auto-generated method stub
		return phoneRepository.findAllByMemberId(memberId);
	}

	@Override
	public List<Member> findAllMemberByNo(String no) {
		// TODO Auto-generated method stub
		List<Phone> phone = phoneRepository.findByNoContaining(no);
		List<Member> member = new ArrayList<Member>();

		for (int i = 0; i < phone.size(); i++) {
			member.add(phone.get(i).getMember());
		}
		return member;
	}

	@Override
	public void genUpdate(int memberId, int phoneId) {
		// TODO Auto-generated method stub
		Member member = memberRepository.findById((long) memberId).get();
		Phone phoneC = phoneRepository.findById((long) phoneId).get();

		ArrayList<Phone> p = phoneRepository.findByGenAndMember(1, member);

		if (p.size() == 0) {
			phoneC.setGen(1);
			phoneRepository.save(phoneC);
		} else if (p.size() > 0) {
			Phone phoneW = p.get(0);
			phoneW.setGen(0);
			phoneRepository.save(phoneW);

			phoneC.setGen(1);
			phoneRepository.save(phoneC);
		}
	}

	@Override
	public List<Member> findAllMemberByGenAndNo(int gen, String no) {
		// TODO Auto-generated method stub
		List<Phone> phone = phoneRepository.findByGenAndNoContaining(1, no);
		List<Member> member = new ArrayList<Member>();

		for (int i = 0; i < phone.size(); i++) {
			member.add(phone.get(i).getMember());
		}
		return member;
	}

//	Dto
	@Override
	public ArrayList<PhoneResponseDto> findAllPhonesByMemberId(int memberId) {
		// TODO Auto-generated method stub
		ArrayList<PhoneResponseDto> phone = new ArrayList<>(phoneRepository.findByMemberId(memberId));
//		return phoneRepository.findByMemberId(memberId);
		return phone;
	}

	@Override
	public List<MemberResponseDto> searchAllMemberByNo(String no) {
		// TODO Auto-generated method stub
		List<Phone> phone = phoneRepository.findByNoContaining(no);
		List<Member> member = new ArrayList<Member>();

		for (int i = 0; i < phone.size(); i++) {
			member.add(phone.get(i).getMember());
		}
		List<MemberResponseDto> members = member.stream().map(MemberResponseDto::new).collect(Collectors.toList());
		
		return members;
	}

	@Override
	public List<MemberResponseDto> searchAllMemberByGenAndNo(int gen, String no) {
		// TODO Auto-generated method stub
		List<Phone> phone = phoneRepository.findByGenAndNoContaining(1, no);
		List<Member> member = new ArrayList<Member>();
		
		for (int i = 0; i < phone.size(); i++) {
			member.add(phone.get(i).getMember());
		}
		
		List<MemberResponseDto> members = member.stream().map(MemberResponseDto::new).collect(Collectors.toList());
		
		return members;
	}

	@Override
	public void save(PhoneRequestDto phoneRequestDto) {
		// TODO Auto-generated method stub		
//		phone.setMember(memberRepository.findById(phoneRequestDto.getMemberId()).get());
		Member member = memberRepository.findById(phoneRequestDto.getMemberId()).get();
		Phone phone = phoneRequestDto.toEntity();
		phone.setMember(member);
		phone.setGen(0);
		phoneRepository.save(phone);
	}

	@Override
	public boolean existsByNo(String no) {
		// TODO Auto-generated method stub
		return phoneRepository.existsByNo(no);
	}

}
