package kr.co.uclick.service;


import java.util.List;

import kr.co.uclick.dto.MemberRequestDto;
import kr.co.uclick.dto.MemberResponseDto;
import kr.co.uclick.entity.Member;


public interface MemberService {
	
//	List<Member> findAll();
	public List<Member> findAllByOrderByIdDesc();
	public List<Member> findByNameContaining(String name);
	
	public void create(String name);
	public void update(int id, String name);
	public void delete(int id);
	public void createNo(String name, String no);

	public List<Member> getById(int id);
	public Member findById(int id);
	
//	Dto
	public void save(MemberRequestDto member);
	
	public MemberResponseDto findOne(int id);
	
	public List<MemberResponseDto> findAll();
	
	public void genUpdate(int memberId, int phoneId); 

	public List<MemberResponseDto> searchByName(String name);

}
