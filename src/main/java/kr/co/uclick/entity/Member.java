package kr.co.uclick.entity;

import java.util.ArrayList;
import java.util.Collection;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.TableGenerator;

import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.springframework.cache.annotation.Cacheable;

  
@Entity
@TableGenerator(name = "member", allocationSize = 1)	// hibernate sequence
@Cacheable
@org.hibernate.annotations.Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
//@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Member {

	@Id
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "member")	// hibernate sequence 
	@Column
	private Long id;

	@Column(length = 20) 	
	private String name;
	
	@Column					
	private String genNo;
	
	// cascade로 member 관련 데이터 동시에 취급
//	@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
	@org.hibernate.annotations.Cache(usage = CacheConcurrencyStrategy.NONE)	 
	@OneToMany(cascade = CascadeType.ALL, mappedBy = "member")				 
	private Collection<Phone> phones;
	
	public Member(String name) { 
		// TODO Auto-generated constructor stub
		this.name = name;
	}
	
	public Member() {	
		
	}
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Collection<Phone> getPhones() {
		if (phones == null) { // 등록된 번호가 없는 경우
			phones = new ArrayList<Phone>();
		}
		return phones;
	}
	
	public void setPhones(Collection<Phone> phones) {
		this.phones = phones;
	}

	public void addPhone(Phone p) { // 번호 추가하는 경우
		Collection<Phone> phones = getPhones();
		p.setMember(this);
		phones.add(p);
	}

	public String getGenNo() {
		return genNo;
	}

	public void setGenNo(String genNo) {
		this.genNo = genNo;
	}
	
}
