package kr.co.uclick.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.TableGenerator;

import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.springframework.cache.annotation.Cacheable;


@Cacheable
@org.hibernate.annotations.Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
//@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Entity
@TableGenerator(name = "phone", allocationSize = 1) // hibernate sequence
public class Phone {	

	@Id
//	@GeneratedValue
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "phone") // hibernate sequence
	@Column
	private Long id;

	@Column
	private String no;	// 전화번호

	@ManyToOne(optional = false)
	@JoinColumn(name = "member_id")
	private Member member;	// 사용ㅇ자

	@Column(nullable = false, columnDefinition = "int(1) default 0")
	private int gen;	

	public Phone() {

	}

	public Phone(String no) { // MemberRepositoryTest 과정에서 생성
		// TODO Auto-generated constructor stub
		this.no = no;
	}
	
	public Phone(String no, int gen) {
		this.no = no;
		this.gen = gen;
	}

	public void setMember(Member member) { // Member.java 과정에서 생성
		// TODO Auto-generated method stub
		this.member = member;		
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNo() {
		return no;
	}

	public void setNo(String no) {
		this.no = no;
	}

	public Member getMember() {
		return member;
	}

	public int getGen() {
		return gen;
	}

	public void setGen(int gen) {
		this.gen = gen;
	}

}
