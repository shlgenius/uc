<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
</head>
<body>
	view.jsp
	<br>
	<c:forEach items="${viewMembers}" var="member">
		<table border="1">
			<tr>
				<td>이름</td>
				<td>${member.name}</td>
			</tr>
			<tr>
				<td>대표</td>
				<td>
			<c:forEach items="${viewPhones}" var="phones">				
				<c:choose>
					<c:when test="${phones.gen == 1}">
						${phones.no}
					</c:when>
				</c:choose>				
			</c:forEach>
				</td>
			</tr>
			
			<c:forEach items="${viewPhones}" var="phones">
			<tr>
				<td>번호</td>
				<td>${phones.no}</td>
				<td>
					<input type="button" value="대표" onClick="location.href='genUpdate?memberId=${member.id}&phoneId=${phones.id}'">
					<input type="button" value="삭제" onClick="location.href='phoneDelete?phoneId=${phones.id}'">
				</td>
			</tr>
			</c:forEach>
			<tr>
				<td>
					<input type="button" value="번호 추가" onclick="location.href='phoneAddForm?memberId=${member.id}'" />
				</td>
			</tr>
			<tr>
				<td>
					<input type=button value="목록" onClick="location.href='list'">
					<input type=button value="사용자 정보 수정" onClick="location.href='editForm?id=${member.id}'">
				</td>
			</tr>
		</table>
	</c:forEach>
</body>
</html>