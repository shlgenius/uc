<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
</head>
<body>
	editForm.jsp
	<br>
	<c:forEach items="${viewMembers}" var="member">
		<!-- <form action="memberUpdate" method="post"> -->
		<form action="memberUpdateDto" method="post">
			<table border="1">
				<tr>
					<td>이름</td>
					<td>
						<input type="text" name="name" value="${member.name}">
						<input type="hidden" name="id" value="${member.id}">
					</td>
				</tr>
				
				<c:if test="${not empty err}">
				<tr>
					<td>${err.defaultMessage}</td>
				</tr>
				</c:if>
			
			</table>
			<input type=submit value="사용자 정보 수정">
		</form>
		<table border="1">

			<c:forEach items="${viewPhones}" var="phones">
			<form method="post" action="phoneUpdate">
				<tr>
					<td>번호</td>
					<!-- <form method="post" action="phoneUpdate"> -->
					<td>
						<input type="text" name="no" value="${phones.no}">
						<input type="hidden" name="phoneId" value="${phones.id}">
						<input type="hidden" name="memberId" value="${member.id}">
					</td>
					<td>
						<input type="submit" value="번호 변경 저장"> 
						<input type=button value="번호 삭제" onClick="location.href='phoneDelete?phoneId=${phones.id}&memberId=${member.id}'">
					</td>
					
				</tr>
			</form>
			</c:forEach>

		</table>
		
		<input type=button value="목록" onClick="location.href='list'"> 
		<input type=button value="취소" onClick="location.href='view?id=${member.id}'"> 
		<input type=button value="사용자 삭제" onClick="location.href='memberDelete?id=${member.id}'">
	</c:forEach>
</body>
</html>