<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/css/black.css" />
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/css/style.css" />

<style type="text/css">
* {
	padding: 0; margin: 0;	
}
html {
	height: 100.1&#37;;	
}
#content1 {
	float: left;
	width: 20%;
	/* background-color: #DDD; */
}
#content2 {
	float: left;
	width: 60%;
	/* background-color: #CCC; */
}
#content3 {
	float: left;
	width: 20%;
	/* background-color: #CCC; */
}

#searchInput {
	float: left;
	width: 60%;
	/* background-color: #DDD; */
}
#selectBox {
	float: left;
	width: 30%;
	/* background-color: #CCC; */
}
#searchButton {
	float: left;
	width: 10%;
	/* background-color: #CCC; */
}
</style>

<script>
function submitSearch() {
	var key = document.getElementById('key');
	var searchString = document.getElementById('searchString');
	if (key == null || searchString == null) {
		return false;
	} else {
		return true;
	}
}

</script>

<title>list.jsp</title>
</head>

<body>
<header class="container theme padding">
	<div class="center" style="padding-bottom: 32px;">
		<h4>UC</h4>
		<a href="list"></a>
	</div>
</header>
	<!-- list.jsp
	<br> -->
	
<div class="container">
	<div class="responsive card-4 row">
	<div class="column">		
	</div>
	<div class="column">	
	<table class="table bordered">
		<tr class="theme">
			<!-- <th style="text-align: center;">사용자</th> -->
			<!-- <th style="text-align: center;">사용자링크테스트</th> -->
			<th style="text-align: center;">Dto상세보기</th>
			<!-- <th style="text-align: center;">번호</th> -->
			<th style="text-align: center;">멤버Entity 대표번호</th>
		</tr>
		
		<c:forEach var="member" items="${members}">
		<tr>			
			<%-- <td class="center padding-8">${member.name}</td> --%>
			<%-- <td><a href="view?id=${member.id}">${member.name}</a></td> --%>
			<td class="center padding-8"><a href="viewDto?id=${member.id}">${member.name}</a></td>
			<%-- <td>
				<c:forEach var="phones" items="${member.phones}">
					<c:choose>
						<c:when test="${phones.gen == 1}">
							${phones.no}
						</c:when>
					</c:choose>
				</c:forEach>
			</td> --%>
			<td>
				${member.genNo}
			</td>
		</tr>
		</c:forEach>
	
	</table>
	</div>
	
	<div class="column">		
	</div>
	</div>
</div>

<!-- <div class="container">
	<div class="responsive card-4">
	<form id="search" action="list" onsubmit="return submitSearch();">
		<input type="text" id="searchString" name="searchString" class="input">
		<select id="key" name="key" class="select border">
			<option selected>선택</option>
			<option value="name">이름</option>
			<option value="no">번호</option>
			<option value="genNo">대표</option>
		</select>
		<input type="submit" value="검색">
	</form>
	</div>
</div> -->

<div class="container">
	<div id="content1">
		<input type=button value="목록" onClick="location.href='list'" class="btn theme"> 	
	</div>
	<div id="content2">
		<form id="search" action="list" onsubmit="return submitSearch();">
				
		<div id="searchInput">
		<input type="text" id="searchString" name="searchString" class="input">
		</div>
		
		<div style="width: 100px;" id="selectBox">
		<select id="key" name="key" style="background-color: #ffffff" class="select">			
			<option selected>선택</option>
			<option value="name">이름</option>
			<option value="no">번호</option>
			<option value="genNo">대표</option>			
		</select>
		</div>
		
		<div id="selectButton">
		<input type="submit" value="검색" class="btn theme">
		</div>
		
		</form>
	</div>
	<div id="content3">
		<input type=button value="사용자 추가" onClick="location.href='newForm'" class="btn theme">	
	</div>
</div>
	
	<input type=button value="목록" onClick="location.href='list'" > 
	<input type=button value="사용자 추가" onClick="location.href='newForm'">
</body>
</html>