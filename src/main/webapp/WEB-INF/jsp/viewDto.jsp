<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %> 
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" type="text/css" href="/resources/css/mystyle.css" />

</head>
<body>
<div style="background-color:#231F20; text-align:center; padding: 4px;">
	<div style="padding-bottom:32px; display:inline-block;width:auto; ">
		<h3><a href="list" style="text-decoration:none; color:#ffffff;">UC</a></h3>
	</div>
</div>
<div class="bodyDiv">
	<div class="bodyFirstDiv relative">
		<div class="overTableDiv">
			전화번호부
			<table class="basicTable">
			
				<tr class="titleTr theme_box">
					<td style="width:138px;" class="han textLeft font15">${member.name}</td>
					<td style="text-align:center; width:494px; text-align:left; padding: 0px 0px 0px 4px;"></td>
					<!-- <td style="text-align:center; width:290px; text-align:left; padding: 0px 0px 0px 4px;"></td> -->
					<td colspan="2" style="text-align:center; width:96px; "></td>
				</tr>
				
				<tr class="noticeTr">
					<!-- <td style="text-align:center;line-height:33px;height:33px; width:96px; font-size:12px; color:#aaaaaa; ">ID</td> -->
					<td colspan="4" class="basicTd relative textCenter font14 help bold" style="-webkit-text-size-adjust:none; padding-right:0; margin-right:2px; letter-spacing:-1px; ">
						<div class="noticeDiv">
	        				<div class="help" title='사실 ?!'></div>
		    			</div>
		    			${member.name} 의 기본 정보
		    		</td>
					<!-- <td></td> -->
				</tr>
				
				<tr class="basicTr relative">
					<td class="basicTd font14 textLeft" style="width:96px; ">아이디</td>
					<td class="basicTd font14 textLeft relative" style="width:246px; ">${member.id}</td>
					<td></td>
					<td></td>
				</tr>
				
				<tr class="basicTr relative">
					<td class="basicTd font14 textLeft" style="width:96px; ">이름</td>
					<td class="basicTd font14 textLeft relative" style="width:246px; ">${member.name}</td>
					<td></td>
					<td></td>
				</tr>
				
				<tr class="basicTr relative">
					<td class="basicTd font14 textLeft" style="width:96px; ">대표 전화</td>
					<td class="basicTd font14 textLeft relative" style="width:246px; ">
						<c:forEach items="${phones}" var="phones">
							${phones.genPhoneNo}
						</c:forEach>
					</td>
					<td></td>
					<td></td>
				</tr>
				
				<tr class="noticeTr">
					<!-- <td style="text-align:center;line-height:33px;height:33px; width:96px; font-size:12px; color:#aaaaaa; ">ID</td> -->
					<td colspan="4" class="basicTd relative textCenter font14 help bold" style="-webkit-text-size-adjust:none; padding-right:0; margin-right:2px; letter-spacing:-1px; ">
						<div class="noticeDiv">
	        				<div class="help" title='사실 ?!'></div>
		    			</div>
		    			${member.name} 의 등록 번호
		    		</td>
					<!-- <td></td> -->
				</tr>
				
				<c:forEach items="${phones}" var="phones">
				<tr class="basicTr relative">
					<td class="basicTd font14 textLeft" style="width:96px; ">등록 번호</td>
					<td class="basicTd font14 textLeft relative" style="width:246px; ">${phones.no}</td>
					<td class="basicTd"><input type="button" value="대표" class="btnSm font14" onClick="location.href='genUpdate?memberId=${member.id}&phoneId=${phones.id}'"></td>
					<td class="basicTd"><input type="button" value="삭제" class="btnSm font14" onClick="location.href='phoneDelete?phoneId=${phones.id}&memberId=${member.id}'"></td>				
				</tr>
				</c:forEach>
				
				<c:if test="${fn:length(phones) < 5}">
				<tr class="basicTr relative">
					<td class="basicTd font14 textLeft" style="width:96px; "></td>
					<td class="basicTd font14 textLeft relative" style="width:246px; ">
						<input type="button" value="&#43; 번호 추가" class="btnLong font14" onclick="location.href='phoneAddDto?id=${member.id}'" />
					</td>
					<td></td>
					<td></td>
				</tr>
				</c:if>
				
			</table>
		
	
			<table class="linkTable">
				<tr>
					<td style="width: 96px; display:block;" align="left" valign="top">
						<!-- <a class="linkList" href="list"> -->
							<input type="button" class="btnSm" onClick="location.href='list'" value="목록">
						<!-- </a> -->
					</td>
					<td align="center" valign="top" style="padding-top: 2px; " width="100%">						
					</td>
					<td width="120px" style="display:block; " align="right" valign="top">
						<input type="button" class="btnLong" onClick="location.href='editFormDto?id=${member.id}'" value="사용자 수정"></input>
					</td>
				</tr>
			</table>
			
			<div class="insertDiv">
			
				<form id="search" action="list" onsubmit="return submitSearch();">
				<div style="position:relative; ">
					<select id="key" name="key" class="selectBox">
						<option selected>선택</option>
						<option value="name">이름</option>
						<option value="no">번호</option>
						<option value="genNo">대표번호</option>
					</select>
						
					<input type="text" id="searchString" name="searchString" class="searchInput" maxlength="50" required autocomplete="off" ></input>
					<input type="submit" class="btnSm" style="height:33px" value="검색"></input>				
				</div>
				</form>
			</div>		
		</div>
	</div>
</div>

</body>
</html>