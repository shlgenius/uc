<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<%-- <link rel="stylesheet" type="text/css"
	href="${pageContext.request.contextPath}/resources/css/black.css" />
<link rel="stylesheet" type="text/css"
	href="${pageContext.request.contextPath}/resources/css/style.css" /> --%>

<style type="text/css">
/* 빨주황색 : #F14E22 */
.theme_box2, .btn, .btn_input, .rc_category_unselected:hover , .theme_point_box1, .list_comment_em:hover {
		background-color: #231F20;
		color: white;
		border-color: #231F20;
		text-shadow: 0 0 5px rgba(0,0,0,0.6);
	}
.theme_box2 a:hover {
		color: #F14E22;
		text-decoration: none;
		text-shadow: none;
	}
.hover {
	text-decoration: none;
}

.btn2 {
	display:inline-block;
	padding:0 11px 2px;
	cursor:pointer;
	line-height:220%;
	border-radius:2px;
	box-shadow:0 0 2px rgba(0,0,0,.25);
}
.btn2:hover {
	text-decoration:none;
}

</style>

<script>
	function submitSearch() {
		var key = document.getElementById('key');
		var searchString = document.getElementById('searchString');
		if (key == null || searchString == null) {
			return false;
		} else {
			return true;
		}
	}
</script>

<title>list.jsp</title>
</head>

<body>
	
<div style="background-color:#F8E0E6; text-align:center; padding: 4px;">
	<div style="padding-bottom: 32px; display:inline-block;width:auto; ">
		<h4>UC</h4>
		<a href="list"></a>
	</div>
</div>

	
<div style="padding:0;position:relative;z-index:0;margin-top:35px;">

	<div style="width:1089px;margin:0 auto 5px; position:relative;">
		<div style="float:left;font-size:27px; display:table; talbe-layout:fixed; ">전화번호부</div>		
	</div>
	
	<div style="width:1089px;margin:0 auto; display:table; table-layout:fixed;">
		<div style="display:table-row;">
			<div style="display:table-cell;vertical-align:top;position:relative;width:728px">
				<div>
					<div style="margin-bottom:6px;width:728px;box-shadow:0 0 3px #bbb;box-shadow:0 0 5px rgba(0,0,0,.2);background:#fff">
					
						<div style="height:42px;line-height:42px;font-size:14px;position:relative;" class="theme_box2">
							<div style="text-align:center;float:left; width:65px">작성시간</div>
							<div style="text-align:center;float:left; width:28px">추천</div>
							<div style="text-align:center;float:left; width:430px;"></div>
							<div style="text-align:center;float:left; width:50px;float:right">조회</div>
							<div style="text-align:center;float:left; float:right;width:153px; text-align: center">닉네임</div>
						</div>
						
						<div style="clear:both;border-bottom:1px solid #f3f3f3;background:#fff;overflow:hidden; position:relative; background:#f8f8f8;border-bottom-color:#e8e8e8;">						
							<div style="float:left;text-align:center;line-height:33px;height:33px; width:65px; font-size:12px; color:#aaa">
								<span>날짜</span>
							</div>
							<div style="float:left;text-align:center;line-height:33px;height:33px; width:28px; font-size:12px; font-weight:700; " class="theme_key2">15</div>							
							<div style="float:left;text-align:center;line-height:33px;height:33px; width:430px; text-align:left;position:relative;" align=left>
								<div style="float:left; -webkit-text-size-adjust:none; padding-right:0;">
									<div style="float:left; display:inline-block;width:24px;text-align:center;font-size:14px;color:#666;line-height:28px;margin-right:2px; ">
										<div style="background-position:-1269px -138px;width:16px;height:16px; cursor:help; " title="공지사항입니다. 꼭 읽어주세요!"></div>
									</div>
									
										<span style="font-weight:700; letter-spacing:-1px; " class="theme_key2">
											<span style="-webkit-text-size-adjust:none; ">
												망할 전화번호부입니다
												<span>
													<em style="border:1px solid #e5e5e5;padding:0 4px;border-radius:9px;min-width:9px;text-align:center;font-style:normal;line-height:17px;background:#fff;display:inline-block; font-size:11px; background:#f8f8f8;color:#666; ">6</em>
												</span>
											</span>
										</span>
								</div>
							</div>
							<div style="float:left;text-align:center;line-height:33px;height:33px; width:50px;float:right; font-size:12px; font-family:'Trebuchet MS',arial,sans-serif;letter-spacing:0; color:#aaa; ">220333</div>
							<div style="float:left;text-align:center;line-height:33px;height:33px; float:right;width:153px; font-weight:700; text-align:left; overflow:hidden;text-overflow:ellipsis;white-space:nowrap; ">
							</div>
						</div>
						
						<c:forEach var="member" items="${members}">
						<div style="clear:both;border-bottom:1px solid #f3f3f3;background:#fff;overflow:hidden; position:relative; ">
						
							<div style="float:left;text-align:center;line-height:33px;height:33px; width:65px; font-size:12px; color:#aaa">
								<span></span>
							</div>
							<div style="float:left;text-align:center;line-height:33px;height:33px; width:28px; font-size:12px; font-weight:700; " class="theme_key2" align="center"></div>
							<div style="float:left;text-align:center;line-height:33px;height:33px; width:430px;text-align:left;position:relative; " align=left>
								<div style="width:130px; -webkit-text-size-adjust:none; ">
									<div style="width:130px; display:inline-block;width:24px;text-align:center;font-size:14px;color:#666;line-height:28px;margin-right:2px; " align=left>
										<div style="background:url(../img/csg-52b4c8a42dbe6.png) no-repeat top left;display:inline-block; background-position:-198px -276px;width:16px;height:16px;margin-bottom:-4px; cursor:help; " title="일반 게시물">
										</div>
									</div>
										<a style="padding:8px 0; " href="viewDto?id=${member.id}">
											<span style="-webkit-text-size-adjust:none; ">${member.name}</span>
										</a>
								</div>
							</div>
							<div style="float:left;text-align:center;line-height:33px;height:33px; width:50px;float:right; font-size:12px; font-family:'Trebuchet MS',arial,sans-serif;letter-spacing:0; color:#aaa; "></div>
							<div style="float:left;text-align:center;line-height:33px;height:33px; float:right;width:153px; font-weight:700; text-align:left; overflow:hidden;text-overflow:ellipsis;white-space:nowrap; color:#555555">
								<a style="" onClick="call_sideview(this, 'rkdalsrb', '강민규', '', '');">
									<i class='help fa fa-heart-o member_icon_heart' title='매니아 커뮤니티의 운영진을 표시하는 마크입니다.'></i>
									<span title="강민규 (rkdalsrb)"><span class='member'>강민규</span></span>
								</a>
							</div>
						</div>
						</c:forEach>
						
					</div>
				</div>
				
				<table style="margin-top:10px; width:728px; cellspacing:0; cellpadding:0; display:table;" >
				<tr>
					<td style="width: 80px; display:block;" align="left" valign="top">
						<a style="display:inline-block;padding:0 11px 2px;cursor:pointer;line-height:220%;font-family:'malgun gothic','Apple SD Gothic Neo',AppleGothic;border-radius:2px;box-shadow:0 0 2px rgba(0,0,0,.25)" href="list">
							<span>목록</span>
						</a>
					</td>
					<td align="center" valign="top" style="padding-top: 2px; " width="100%">						
					</td>
					<td width="120px" style="display:block; " align="right" valign="top">
						<a class="btn2" href="newForm">
							<span><i style="margin-bottom:1px;">사용자 추가</i></span>
						</a>
					</td>
				</tr>
				</table>
				
				<div style="position:relative;text-align:center;margin-top:30px;width:728px; ">
					<form id="search" action="list" onsubmit="return submitSearch();">
					<div style="position:relative; ">
						<select id="key" name="key" style="background-color: #ffffff">
						<option selected>선택</option>
						<option value="name">이름</option>
						<option value="no">번호</option>
						<option value="genNo">대표</option>
						</select>
						
						<input type="text" id="searchString" name="searchString" style="padding:5px 30px 5px 14px;line-height:13px;width:210px;border:0;background:#fff;color:#404040;border-radius:14px; box-shadow:0 0 3px #bbb;box-shadow:0 0 5px rgba(0,0,0,.2);background:#fff; border-radius:8px; " maxlength="50" required autocomplete="off" ></input>
						<input type="submit" style="background:url(../img/csg-52b4c8a42dbe6.png) no-repeat top left;display:inline-block; background-position:-1376px -276px;width:30px;height:30px; border:none;padding:0;cursor:pointer;position:absolute;top:-2px;right:50%;margin-right:-126px;text-indent:-9999px; "></input>				
					</div>
					</form>
				</div>
				
			</div>
		</div>
	</div>
	
</div>


<div style="position:relative; position:absolute; bottom:0; background-color:#F8E0E6; text-align:center; padding: 4px;">

	<div style="position:relative;z-index:1; transition:all .2s ease-in-out; box-shadow:0 0 10px #bbb;box-shadow:0 0 10px rgba(0,0,0,.2);background:#fff; padding-bottom: 32px; display:inline-block;width:auto; ">
		<div style="width:1089px;margin:0 auto;padding:10px 0;letter-spacing:-1px; font-family:Malgun Gothic,Dotum,Gulim,'Apple SD Gothic Neo',AppleGothic; ">
			<a style="position:relative;display:inline-block;margin:0 20px; " href="list">
				<span>공지사항</span>
			</a>
			<a style="position:relative;display:inline-block;margin:0 20px; " href="/g2/bbs/board.php?bo_table=site_introduction&r=ok"><span >사이트소개</span></a>  
			<a style="position:relative;display:inline-block;margin:0 20px; " href="/g2/bbs/board.php?bo_table=public_sphere&r=ok">
				<span class="bold ">운영공론장</span>
					<div style="position:absolute;top:-6px;right:-31px;width:30px;font-family:'Trebuchet MS';font-size:10px;text-align:left;font-weight:700; ">1</div>
			</a>
			<a style="position:relative;display:inline-block;margin:0 20px; text-decoration:none;" href="/g2/bbs/board.php?bo_table=greensmile&r=ok">
				<span class="">그린스마일</span>
			</a>
			<a style="position:relative;display:inline-block;margin:0 20px; text-decoration:none; font-weight:700; " href="/g2/bbs/board.php?bo_table=participation&r=ok">
				<span class="">제안/문의</span>
			</a>
			<a style="position:relative;display:inline-block;margin:0 20px; text-decoration:none; " href="/g2/module/recent_executions/index.php">
				<span>최근운영내역</span>
			</a>
			<a style="position:relative;display:inline-block;margin:0 20px; text-decoration:none; " href="/g2/bbs/board.php?bo_table=site_introduction&wr_id=32">
				<span >개인정보취급방침</span>
			</a>
		</div>				
	</div>
	
	<div style="padding:30px 0 35px;position:relative;z-index:2; font-size:12px; font-family:'Trebuchet MS',arial,sans-serif;letter-spacing:0; " class="theme_box2">
		Copyright ⓒ Mania Community.<span> All rights reserved.</span>
	</div>
</div>

<div style="position:fixed;top:0;width:50%;height:100%;z-index:8000;-webkit-user-select:none;-moz-user-select:none;-khtml-user-select:none;-ms-user-select:none; left:-544.5px; ">
	<div style="left:auto;right:50%;margin-right:600px; "></div>
</div>
<div style="position:fixed;top:0;width:50%;height:100%;z-index:8000;-webkit-user-select:none;-moz-user-select:none;-khtml-user-select:none;-ms-user-select:none; left:-544.5px; ">
	<div style="left:50%;right:auto;margin-left:600px; "></div>
</div>
	<a id="quick_bottom"></a>
<div class="theme_box2"></div>


</body>
</html>