<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" type="text/css" href="/resources/css/mystyle.css" />

<script>
	function submitSearch() {
		var key = document.getElementById('key');
		var searchString = document.getElementById('searchString');
		if (key == null || searchString == null) {
			return false;
		} else {
			return true;
		}
	}
</script>

<title>list.jsp</title>
</head>

<body>
	
<div style="background-color:#231F20; text-align:center; padding: 4px;">
	<div style="padding-bottom:32px; display:inline-block;width:auto; ">
		<h3><a href="list" style="text-decoration:none; color:#ffffff;">UC</a></h3>
	</div>
</div>

<div style="padding:0; position:relative; z-index:0; margin-top:32px;">
	<div style="width: 1089px; margin:0 auto 5px; position:relative;">
		<div style="float:left; font-size:27px; display:table; table-layout:fixed; ">
			전화번호부
			<table style="margin:0 auto; vertical-align:top; position:realtive; width:728px; table-layout:fixed; white-space:nowrap; overflow:hidden; border-collapse:collapse;" >
			
				<tr style="height:42px; line-height:42px; font-size:14px; position:relative;" class="theme_box">
					<td style="text-align:center; width:96px;">ID</td>
					<td style="text-align:center; width:246px; text-align:left; padding: 0px 0px 0px 4px;">이름</td>
					<td style="text-align:center; width:290px; text-align:left; padding: 0px 0px 0px 4px;">대표번호</td>
					<td style="text-align:center; width:96px; ">상세보기</td>
				</tr>
				
				<tr style="clear:both; border-bottom:1px solid #f3f3f3; background:#ffffff; overflow:hidden; position:relative; background:#f8f8f8; border-bottom-color:#e8e8e8; ">
					<td style="text-align:center;line-height:33px;height:33px; width:96px; font-size:12px; color:#aaaaaa; "><!-- ID --></td>
					<td colspan="2" style="text-align:center;line-height:33px;height:33px; position:relative; -webkit-text-size-adjust:none; padding-right:0; text-align:center; font-size:14px; margin-right:2px; cursor:help; font-weight:700; letter-spacing:-1px; ">
						<div style="display:inline-block;width:24px;text-align:center;font-size:14px;color:#666;line-height:28px;margin-right:2px">
	        				<div style="cursor:help; " title='사실 ?!'></div>
		    			</div>
		    			전화번호부입니다.
		    		</td>
					<td></td>										
				</tr>
				
				<c:forEach var="member" items="${members}">
				<tr style="clear:both;border-bottom:1px solid #f3f3f3;background:#fff;overflow:hidden; position:relative; ">
					<td style="text-align:center;line-height:33px;height:33px; width:96px; font-size:14px; ">${member.id}</td>
					<td style="text-align:center;line-height:33px;height:33px; width:246px; text-align:left; font-size:14px; position:relative; "><a style="curosr:pointer; text-decoration:none; color:#000000; " href="viewDto?id=${member.id}">${member.name}</a></td>
					<td style="text-align:center; width:290px; font-size:14px; text-align:left; ">${member.genNo}</td>
					<td style="padding: 0px 0px 3px 0px; "><input type="button" class="btnOneView font14" value="상세보기" onClick="location.href='viewDto?id=${member.id}'"/></td>
				</tr>
				</c:forEach>
				
			</table>
		
	
			<table class="linkTable">
				<tr>
					<td style="width: 96px; display:block;" align="left" valign="top">
						<!-- <a class="linkList" href="list"> -->
							<input type="button" class="btnSm" onClick="location.href='list'" value="목록">
						<!-- </a> -->
					</td>
					<td align="center" valign="top" style="padding-top: 2px; " width="100%">						
					</td>
					<td width="120px" style="display:block; " align="right" valign="top">
						<input type="button" class="btnLong" onClick="location.href='newForm'" value="사용자 추가">
					</td>
				</tr>
			</table>
			
			<div class="insertDiv">
			
				<form id="search" action="list" onsubmit="return submitSearch();">
				<div style="position:relative; ">
					<select id="key" name="key" class="selectBox">
						<option selected>선택</option>
						<option value="name">이름</option>
						<option value="no">번호</option>
						<option value="genNo">대표번호</option>
					</select>
						
					<input type="text" id="searchString" name="searchString" class="searchInput" maxlength="50" required autocomplete="off" ></input>
					<input type="submit" class="btnSm" style="height:33px" value="검색"></input>				
				</div>
				</form>
			</div>
						
		</div>
	</div>
</div>


</body>
</html>